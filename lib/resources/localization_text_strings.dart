//Choose App Language
const strChooseApplicationLanguage = "strChooseApplicationLanguage";
const strYourLanguage = "strYourLanguage";
//Login
const strSignInSp = "Log in";
const strWelcome = "Hi Welcome\nto Casa Rancha";
const strPleaseSignInYouAccount = "Please login your account.";
const strForgotPassword = "Forgot Password?";
const strWith = "With";
const strYouDontHaveAccount = "Don’t have an account?";
const strSignUpSp = "Sign up";
const strRexFamily = "Rex & Mitogo family";
//SignUp
const strCreateYourNewAccount = "Create your New Account!";
const strHaveAnAccount = "Already have an account?";
const strMonth = "Month";
const strDay = "Day";
const strYear = "Year";
//Otp Verify
const strOtpVerify = "OTP Verification";
const strSendYou4Digit = "We have sent you 4-digit";
const strOtp = "Otp";
const strOtpOnMail = "Stark@gmail.com";
const strConfirm = "Confirm";
const strResendOtp = "Resend OTP";
const strBackToLogin = "Back to the";

//forgot password
const strEnterEmail = "Please Enter your email\nwhich associate to your account";
const strSendNow = "Send Now";

//Home Screen
const strCasaRanch = "Casa Rancha";

//Button
const strNext = "strNext";
const strSignUp = "SignUp";
const strLogin = "Login";
const strContinue = "Continue";

//Hint
const strEmailAddress = "Email Address";
const strUserName = "Username";
const strPassword = "Password";
const strConfirmPassword = "Confirm Password";
const strFirstName = "First Name";
const strLastName = "Last Name";
const strBio = "Bio";
const strName = "strName";

//appbar title
const strSetProfileBar = "Set Up Profile";