const imgLoginBg = "assets/images/img_login_bg.png";
const imgSignBack = "assets/images/ic_sign_back.png";
const icUser = "assets/images/ic_user.svg";
const icDeselectEmail = "assets/images/ic_deselect_mail.svg";
const icDeselectLock = "assets/images/ic_deselect_lock.svg";
const icSelectEmail = "assets/images/ic_select_mail.svg";
const icSelectLock = "assets/images/ic_select_lock.svg";

const icLeftArrow = "assets/images/ic_left_arrow.svg";
const icDeselectUser = "assets/images/ic_deselect_user.svg";
const icSelectUser = "assets/images/ic_select_user.svg";
const icDropDown = "assets/images/ic_drop_down.svg";
const icProfileAdd = "assets/images/ic_profile_add.svg";
const imgProfile= "assets/images/img_profile.png";

const imgOtpBackground = "assets/images/ic_otp_backgnd.png";
const imgOtpMsgBackground = "assets/images/ic_otp_msg_backgnd.png";

const imgAppLogo = "assets/images/ic_user.svg";

const imgGoogleSignIn = "assets/images/ic_google_sign_in.svg";
const imgAppleSignIn = "assets/images/ic_apple_sign_in.svg";
const imgFaceBookSignIn = "assets/images/ic_facebook_sign_in.svg";
const imgTwitterSignIn = "assets/images/ic_twitter_sign_in.svg";

//HomePage
const icGhostMode = "assets/images/img_home_page/ic_ghost_mode.svg";
const icCardMenu = "assets/images/img_home_page/ic_menu_bar.svg";
const icVerifyBadge = "assets/images/img_home_page/ic_verify_badge.svg";
const icSharePost = "assets/images/img_home_page/ic_comment_post.svg";
const icCommentPost = "assets/images/img_home_page/ic_comment_post.svg";
const icLikeWhite = "assets/images/img_home_page/ic_like_white.svg";
const icLikeRed = "assets/images/img_home_page/ic_like_red.svg";