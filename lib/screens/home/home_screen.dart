import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../base/base_stateful_widget.dart';
import '../../resources/color_resources.dart';
import '../../resources/image_resources.dart';
import '../../resources/localization_text_strings.dart';
import '../../view_models/home_vm/home_screen_view_model.dart';
import '../../widgets/common_appbar.dart';
import '../../widgets/home_page_widgets.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends BaseStatefulWidgetState<HomeScreen> {
  HomeScreenViewModel? homeScreenViewModel;
  @override
  Color? get scaffoldBgColor => colorWhite;
  @override
  bool get shouldHaveSafeArea => true;
  @override
  bool get isBackgroundImage => true;
  @override
  bool get extendBodyBehindAppBar => true;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }
  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return  whiteBgAppBar(elevation: 2,title: strCasaRanch,
        fontWeight : FontWeight.w700 , fontSize: 22.sp , leadIconSvg:icGhostMode , leadHorPadding :5 );
  }

  @override
  Widget buildBody(BuildContext context) {
    homeScreenViewModel = Provider.of<HomeScreenViewModel>(context);
    return WillPopScope(
     onWillPop:()=> Future.value(false),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: SingleChildScrollView(
              child: Column(
                children: [

                  feedCardHome(width: constraints.maxWidth , imgUserNet:
                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeGC2zO9XphU0L4bmk9_Htveabe0LkIuQSgWpb5G_P&s" ,
                  userName: "Codyb.842",
                    lastSeen: "5 min ago at New York",
                    isVerify: false,
                    isLikedPost: false,
                    imgPost:
                    "https://thumbs.dreamstime.com/b/beautiful-rain-forest-ang-ka-nature-trail-doi-inthanon-national-park-thailand-36703721.jpg",

                  ),
                ],
              )
            ),
          );
        },
      ),
    );
  }
}
