import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../base/base_stateful_widget.dart';
import '../../resources/color_resources.dart';
import '../../resources/image_resources.dart';
import '../../resources/localization_text_strings.dart';
import '../../resources/strings.dart';
import '../../view_models/set_profile_sc_view_model.dart';
import '../../widgets/common_appbar.dart';
import '../../widgets/common_button.dart';
import '../../widgets/common_widgets.dart';
import '../../widgets/text_editing_widget.dart';
import '../../widgets/text_widget.dart';

class SetProfileScreen extends StatefulWidget {
  const SetProfileScreen({Key? key}) : super(key: key);

  @override
  State<SetProfileScreen> createState() => _SetProfileScreenState();
}

class _SetProfileScreenState extends BaseStatefulWidgetState<SetProfileScreen> {

  SetProfileScViewModel? setProfileScViewModel;
  @override
  void initState() {
    statusBarDarkMode();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Color? get scaffoldBgColor => colorWhite;

  @override
  bool get shouldHaveSafeArea => false;

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return whiteBgAppBar(title: strSetProfileBar, elevation: 1);
  }

  void _bioTextChange(BuildContext context ) {
    Provider.of<SetProfileScViewModel>(context, listen: false).getBioTextCount();
  }
  @override
  Widget buildBody(BuildContext context) {
     setProfileScViewModel =  Provider.of<SetProfileScViewModel>(context);
        // context.read<SetProfileScViewModel>();
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    heightBox(37.h),
                    SizedBox(
                      height: 127.h,
                      width: 127.w,
                      child: Stack(
                        clipBehavior: Clip.none,
                        fit: StackFit.expand,
                        children: [
                          CircleAvatar(
                              radius: 20,
                              backgroundImage: Image.asset(imgProfile).image),
                          Positioned(
                              bottom: 5.h,
                              right: 8.w,
                              child: SvgPicture.asset(icProfileAdd))
                        ],
                      ),
                    ),
                    heightBox(31.h),
                    TextEditingWidget(
                      controller: setProfileScViewModel!.firstNameController,
                      hintColor: color080,
                      isShadowEnable: false,
                      hint: strFirstName,
                      color: colorFF4,
                      textInputType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () =>
                          FocusScope.of(context).nextFocus(),
                    ),
                    heightBox(9.h),
                    TextEditingWidget(
                      controller: setProfileScViewModel!.lastNameController,
                      hintColor: color080,
                      isShadowEnable: false,
                      hint: strLastName,
                      color: colorFF4,
                      textInputType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      onEditingComplete: () =>
                          FocusScope.of(context).nextFocus(),
                    ),
                    heightBox(9.h),

                    SizedBox(
                      height: 156.h,
                      child: TextFormField(
                        controller: setProfileScViewModel!.bioController,
                        onChanged: (value){
                          _bioTextChange(context);
                        },
                        style: TextStyle(
                          color: color239,
                          fontSize:  16.sp,
                          fontFamily:  strFontName,
                          fontWeight:  FontWeight.w600,
                        ),
                        inputFormatters: [
                           LengthLimitingTextInputFormatter(100),
                        ],
                        cursorColor: color239,
                        decoration:  InputDecoration(
                          filled: true,
                          fillColor: colorFF4 ,
                          suffixIcon: Container(
                            padding: const EdgeInsets.all(10),
                              width: 70.w,
                              alignment: Alignment.bottomCenter,
                              height: constraints.maxHeight,
                              child:TextWidget(
                                text:  "${setProfileScViewModel!.bioTxtCount}/100",
                                fontSize: 14.sp,
                                color: color080,
                                fontWeight: FontWeight.w400,
                              ),

                          ),
                          hintText: strBio,
                          hintStyle: TextStyle(
                            color:  const Color(0xFF3B3B3B).withOpacity(0.5),
                            fontSize: 16.sp,
                            fontFamily:  strFontName,
                            fontWeight:  FontWeight.w300,
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16.0),
                              borderSide: BorderSide.none),
                        ),
                        maxLines: 5,
                        onEditingComplete: () => FocusScope.of(context).unfocus(),
                      ),
                    ),
                    heightBox(180.h),
                    CommonButton(
                      text: strContinue,
                      width: constraints.maxWidth,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
