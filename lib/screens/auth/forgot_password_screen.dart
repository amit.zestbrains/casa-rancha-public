import 'package:casarancha/resources/image_resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../base/base_stateful_widget.dart';
import '../../resources/color_resources.dart';
import '../../resources/localization_text_strings.dart';
import '../../view_models/forgot_pwd_model_view.dart';
import '../../widgets/common_appbar.dart';
import '../../widgets/common_button.dart';
import '../../widgets/common_widgets.dart';
import '../../widgets/text_editing_widget.dart';
import '../../widgets/text_widget.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState
    extends BaseStatefulWidgetState<ForgotPasswordScreen> {
  ForgotPwdScreenViewModel?  forgotPwdScreenViewModel;

  void _emailFocusChange(BuildContext context ) {
    Provider.of<ForgotPwdScreenViewModel>(context, listen: false).setEmailFocusChange();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Color? get scaffoldBgColor => colorWhite;
  @override
  bool get shouldHaveSafeArea => false;
  @override
  bool get isBackgroundImage => true;
  @override
  bool get extendBodyBehindAppBar => true;

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return  onlyLeadIcAppbar(onTap : goBack);
  }

  @override
  Widget buildBody(BuildContext context) {
    forgotPwdScreenViewModel = Provider.of<ForgotPwdScreenViewModel>(context);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraints.maxHeight),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                    width: constraints.maxWidth,
                    child: Image.asset(imgOtpBackground)),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 7.h),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextWidget(
                        text: strForgotPassword,
                        fontSize: 26.sp,
                        color: color13F,
                        fontWeight: FontWeight.w800,
                      ),
                      heightBox(4.h),
                      TextWidget(
                        textAlign: TextAlign.center,
                        text: strEnterEmail,
                        fontSize: 16.sp,
                        color: color080,
                        fontWeight: FontWeight.w400,
                      ),
                      heightBox(42.h),
                      Focus(
                        focusNode: forgotPwdScreenViewModel!.emailFocus ,
                        onFocusChange: (hasFocus){
                          _emailFocusChange(context );
                        },
                        child: TextEditingWidget(
                          controller: forgotPwdScreenViewModel!.emailController,
                          isShadowEnable: false,
                          hint: strEmailAddress,
                          color: forgotPwdScreenViewModel!.emailFillClr ?? colorFF3 ,
                          fieldBorderClr: forgotPwdScreenViewModel!.emailBorderClr,
                          textInputType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          // onFieldSubmitted: ,
                          onEditingComplete: () =>
                              FocusScope.of(context).unfocus(),
                          suffixIconWidget: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10.w),
                            child: SvgPicture.asset(
                                forgotPwdScreenViewModel!.icEmailSvg ??icDeselectEmail),
                          ),
                        ),
                      ),
                      heightBox(42.h),
                      CommonButton(
                        text: strSendNow,
                        width: constraints.maxWidth,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
