import 'package:casarancha/screens/auth/sign_up_screen.dart';
import 'package:casarancha/view_models/login_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../base/base_stateful_widget.dart';
import '../../resources/color_resources.dart';
import '../../resources/image_resources.dart';
import '../../resources/localization_text_strings.dart';
import '../../widgets/common_appbar.dart';
import '../../widgets/common_button.dart';
import '../../widgets/common_widgets.dart';
import '../../widgets/text_editing_widget.dart';
import '../../widgets/text_widget.dart';
import '../home/home_screen.dart';
import 'forgot_password_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends BaseStatefulWidgetState<LoginScreen> {

  @override
  Color? get scaffoldBgColor => colorWhite;
  @override
  bool get shouldHaveSafeArea => false;
  @override
  bool get isBackgroundImage => true;
  @override
  bool get extendBodyBehindAppBar => true;

  LoginScreenViewModel? loginScreenViewModel;

  @override
  void initState() {
    // TODO: implement initState
    statusBarDarkMode();
    // final newVersion = NewVersion(
    //   iOSId: 'com.zb.casarancha.casarancha',
    //   androidId: 'com.zb.casarancha.casarancha',
    // );
    //
    // basicStatusCheck(NewVersion newVersion) {
    //   newVersion.showAlertIfNecessary(context: context);
    // }
    // const simpleBehavior = true;
    // if (simpleBehavior) {
    //   basicStatusCheck(newVersion);
    // }
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    loginScreenViewModel!.emailFocus.dispose();
    loginScreenViewModel!.emailController.dispose();
    loginScreenViewModel!.passwordController.dispose();
    loginScreenViewModel!.passwordFocus.dispose();
  }

  void _emailFocusChange(BuildContext context) {
    Provider.of<LoginScreenViewModel>(context, listen: false).setEmailFocusChange();
  }

  void _pwdFocusChange(BuildContext context) {
    Provider.of<LoginScreenViewModel>(context, listen: false).setPwdFocusChange();
  }



  @override
  Widget buildBody(BuildContext context) {
     loginScreenViewModel = Provider.of<LoginScreenViewModel>(context);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraints.maxHeight),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                    width: constraints.maxWidth,
                    child: Image.asset(imgSignBack)),
                Padding(
                  padding:
                      EdgeInsets.only(left: 20.w, right : 20.w ,bottom: 7.h),
                  child: Form(
                    key : loginScreenViewModel!.globalLoginFormKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: TextWidget(
                            text: strWelcome,
                            fontSize: 26.sp,
                            color: color13F,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        heightBox(8.h),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: TextWidget(
                            text: strPleaseSignInYouAccount,
                            fontSize: 16.sp,
                            color: color080,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        heightBox(30.h),
                        Focus(
                          focusNode: loginScreenViewModel!.emailFocus ,
                          onFocusChange: (hasFocus){
                            _emailFocusChange(context);
                          },
                          child: TextEditingWidget(
                            controller: loginScreenViewModel!.emailController,
                            hint: strEmailAddress,
                            color: loginScreenViewModel!.emailFillClr ?? colorFF3,
                            fieldBorderClr: loginScreenViewModel!.emailBorderClr,
                            textInputType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (val)=>
                                FocusScope.of(context).nextFocus(),
                            onEditingComplete: () =>
                                FocusScope.of(context).nextFocus(),
                            suffixIconWidget: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.w),
                              child: SvgPicture.asset(
                                  loginScreenViewModel!.icEmailSvg ??
                                      icDeselectEmail),
                            ),
                          ),
                        ),
                        heightBox(16.h),
                        Focus(
                          focusNode: loginScreenViewModel!.passwordFocus ,
                          onFocusChange: (hasFocus){
                            _pwdFocusChange(context );
                          },
                          child: TextEditingWidget(

                            controller: loginScreenViewModel!.passwordController,
                            hint: strPassword,
                            fieldBorderClr: loginScreenViewModel!.passwordBorderClr,
                            textInputType: TextInputType.visiblePassword,
                            textInputAction: TextInputAction.go,
                            // onFieldSubmitted: (str) =>
                            //     push(context, enterPage: const SignUpScreen()),
                            passwordVisible: loginScreenViewModel!.passwordVisible,
                            color: loginScreenViewModel!.passwordFillClr ?? colorFF3,
                            onEditingComplete: () =>
                                FocusScope.of(context).unfocus(),
                            suffixIconWidget: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.w),
                              child: GestureDetector(
                                onTap: () =>
                                    loginScreenViewModel!.setPwdVisibility(),
                                child: SvgPicture.asset(
                                    loginScreenViewModel!.icPasswordSvg ??
                                        icDeselectLock),
                              ),
                            ),
                          ),
                        ),
                        heightBox(24.h),
                        Align(
                          alignment: Alignment.center,
                          child: GestureDetector(
                            onTap: () => push(context,
                                enterPage: const ForgotPasswordScreen()),
                            child: TextWidget(
                              text: strForgotPassword,
                              fontSize: 14.sp,
                              color: color080,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        heightBox(24.h),
                        CommonButton(
                          onTap: (){
                            push(context, enterPage: const HomeScreen());
                          },
                          text: strSignInSp,
                          width: constraints.maxWidth,
                        ),
                        heightBox(26.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            horizonLine(
                                width: constraints.maxWidth / 3.4,
                                horizontalMargin: 18.w),
                            TextWidget(
                              text: strWith,
                              fontSize: 14.sp,
                              color: color080,
                              fontWeight: FontWeight.w500,
                            ),
                            horizonLine(
                                width: constraints.maxWidth / 3.4,
                                horizontalMargin: 18.w),
                          ],
                        ),
                        heightBox(26.h),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 36.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              svgImgButton(
                                svgIcon: imgGoogleSignIn,
                              ),
                              svgImgButton(
                                svgIcon: imgTwitterSignIn,
                              ),
                              svgImgButton(
                                svgIcon: imgAppleSignIn,
                              ),
                              svgImgButton(
                                svgIcon: imgFaceBookSignIn,
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextWidget(
                              text: strYouDontHaveAccount,
                              fontSize: 12.sp,
                              color: color080,
                              fontWeight: FontWeight.w500,
                            ),
                            widthBox(4.w),
                            TextWidget(
                              onTap: () =>
                                  push(context, enterPage: const SignUpScreen()),
                              text: strSignUp,
                              fontSize: 12.sp,
                              color: color13F,
                              fontWeight: FontWeight.w600,
                            ),
                          ],
                        ),
                        TextWidget(
                          onTap: () {},
                          text: strRexFamily,
                          fontSize: 14.sp,
                          color: colorPrimaryA05,
                          fontWeight: FontWeight.w400,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
