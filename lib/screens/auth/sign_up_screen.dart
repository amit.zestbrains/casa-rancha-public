import 'package:casarancha/resources/image_resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../base/base_stateful_widget.dart';
import '../../resources/color_resources.dart';
import '../../resources/localization_text_strings.dart';
import '../../view_models/signup_screen_view_model.dart';
import '../../widgets/common_appbar.dart';
import '../../widgets/common_button.dart';
import '../../widgets/common_widgets.dart';
import '../../widgets/text_editing_widget.dart';
import '../../widgets/text_widget.dart';
import 'otp_verification_screen.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends BaseStatefulWidgetState<SignUpScreen> {

  @override
  Color? get scaffoldBgColor => colorWhite;
  @override
  bool get shouldHaveSafeArea => false;
  @override
  bool get isBackgroundImage => true;
  @override
  bool get extendBodyBehindAppBar => true;


  SignupScreenViewModel? signupScreenViewModel;
  void _usernameFocusChange(BuildContext context ) {
    Provider.of<SignupScreenViewModel>(context, listen: false).setUsernameFocusChange();
  }
  void _emailFocusChange(BuildContext context ) {
    Provider.of<SignupScreenViewModel>(context, listen: false).setEmailFocusChange();
  }

  void _pwdFocusChange(BuildContext context ) {
    Provider.of<SignupScreenViewModel>(context, listen: false).setPwdFocusChange();
  }
  void _confirmPwdFocusChange(BuildContext context ) {
    Provider.of<SignupScreenViewModel>(context, listen: false).setConfirmPwdFocusChange();
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  
  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return  onlyLeadIcAppbar(onTap : goBack);
  }

  @override
  Widget buildBody(BuildContext context) {
    signupScreenViewModel = Provider.of<SignupScreenViewModel>(context);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraints.maxHeight),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                    width: constraints.maxWidth,
                    child: Image.asset(imgSignBack)),
                Padding(
                  padding:
                  EdgeInsets.symmetric(horizontal: 20.w, vertical: 7.h),
                  child: Form(
                    key: signupScreenViewModel!.globalSignupFormKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: TextWidget(
                            text: strSignUpSp,
                            fontSize: 26.sp,
                            color: color13F,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        heightBox(8.h),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: TextWidget(
                            text: strCreateYourNewAccount,
                            fontSize: 16.sp,
                            color: color080,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        heightBox(24.h),
                        Focus(
                          focusNode: signupScreenViewModel!.userNameFocus ,
                          onFocusChange: (hasFocus){
                            _usernameFocusChange(context);
                          },
                          child: TextEditingWidget(
                            controller: signupScreenViewModel!.usernameController,
                            isShadowEnable: false,
                            hint: strUserName,
                            color: signupScreenViewModel!.usernameFillClr ?? colorFF3,
                            fieldBorderClr:  signupScreenViewModel!.usernameBorderClr,
                            textInputType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (val)=>
                                FocusScope.of(context).nextFocus(),
                            onEditingComplete: () =>
                                FocusScope.of(context).nextFocus(),
                            suffixIconWidget: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.w),
                              child: SvgPicture.asset(signupScreenViewModel!.icUsernameSvg ?? icDeselectUser),
                            ),
                          ),
                        ),
                        heightBox(18.h),
                        Focus(
                          focusNode: signupScreenViewModel!.emailFocus ,
                          onFocusChange: (hasFocus){
                            _emailFocusChange(context);
                          },
                          child: TextEditingWidget(
                            controller: signupScreenViewModel!.emailController,
                            isShadowEnable: false,
                            hint: strEmailAddress,
                            color: signupScreenViewModel!.emailFillClr ??  colorFF3,
                            fieldBorderClr: signupScreenViewModel!.emailBorderClr,
                            textInputType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (val)=>
                                FocusScope.of(context).nextFocus(),
                            onEditingComplete: () =>
                                FocusScope.of(context).nextFocus(),
                            suffixIconWidget: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.w),
                              child: SvgPicture.asset(signupScreenViewModel!.icEmailSvg ??  icDeselectEmail),
                            ),
                          ),
                        ),
                        heightBox(18.h),
                        Row(
                          children: [
                            dateDropDown(height: 56.h , text: strMonth),
                            widthBox(7),
                            dateDropDown(height: 56.h , text: strDay),
                            widthBox(7),
                            dateDropDown(height: 56.h , text: strYear),
                          ],
                        ),
                        heightBox(18.h),
                        Focus(
                          focusNode: signupScreenViewModel!.passwordFocus ,
                          onFocusChange: (hasFocus){
                            _pwdFocusChange(context);
                          },
                          child: TextEditingWidget(
                            controller: signupScreenViewModel!.passwordController,
                            hint: strPassword,
                            isShadowEnable: false,
                            fieldBorderClr: signupScreenViewModel!.passwordBorderClr,
                            passwordVisible: signupScreenViewModel!.passwordVisible,
                            textInputType: TextInputType.visiblePassword,
                            textInputAction: TextInputAction.next,
                            color: signupScreenViewModel!.passwordFillClr ?? colorFF3,
                            onFieldSubmitted: (val)=>
                                FocusScope.of(context).nextFocus(),
                            onEditingComplete: () =>
                                FocusScope.of(context).nextFocus(),
                            suffixIconWidget: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.w),
                              child: GestureDetector(
                                onTap: () => signupScreenViewModel!.setPwdVisibility(),
                                child: SvgPicture.asset(signupScreenViewModel!.icPasswordSvg ?? icDeselectLock),
                              ),
                            ),
                          ),
                        ),
                        heightBox(18.h),
                        Focus(
                          focusNode: signupScreenViewModel!.confirmPwdFocus ,
                          onFocusChange: (hasFocus){
                            _confirmPwdFocusChange(context);
                          },
                          child: TextEditingWidget(
                            controller: signupScreenViewModel!.confirmPwdController,
                            hint: strConfirmPassword,
                            isShadowEnable: false,
                            fieldBorderClr: signupScreenViewModel!.confirmPwdBorderClr,
                            passwordVisible: signupScreenViewModel!.confirmPasswordVisible,
                            textInputType: TextInputType.visiblePassword,
                            textInputAction: TextInputAction.go,
                            color: signupScreenViewModel!.confirmPwdFillClr ??  colorFF3,
                            onFieldSubmitted: (val)=>push(context, enterPage: const OtpVerifyScreen()),
                            onEditingComplete: () =>
                                FocusScope.of(context).unfocus(),
                            suffixIconWidget: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.w),
                              child: GestureDetector(
                                onTap: () =>signupScreenViewModel!.setConfirmPwdVisibility(),
                                child: SvgPicture.asset(signupScreenViewModel!.icConfirmPwdSvg ??
                                    icDeselectLock),
                              ),
                            ),
                          ),
                        ),
                        heightBox(38.h),

                        CommonButton(
                          text: strSignUpSp,
                          width: constraints.maxWidth,
                          onTap: ()=>push(context, enterPage: const OtpVerifyScreen()),
                        ),
                        heightBox(45.h),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextWidget(
                              text: strHaveAnAccount,
                              fontSize: 12.sp,
                              color: color080,
                              fontWeight: FontWeight.w500,
                            ),
                            widthBox(4.w),
                            TextWidget(
                              onTap: ()=> goBack(),
                              text: strLogin,
                              fontSize: 12.sp,
                              color: color13F,
                              fontWeight: FontWeight.w600,
                            ),
                          ],
                        ),
                        TextWidget(
                          onTap: () {},
                          text: strRexFamily,
                          fontSize: 14.sp,
                          color: colorPrimaryA05,
                          fontWeight: FontWeight.w400,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }


}
