import 'package:casarancha/resources/image_resources.dart';
import 'package:casarancha/screens/auth/set_profile_screen.dart';
import 'package:casarancha/view_models/otp_verify_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';


import '../../base/base_stateful_widget.dart';
import '../../resources/color_resources.dart';
import '../../resources/localization_text_strings.dart';
import '../../resources/strings.dart';
import '../../widgets/common_appbar.dart';
import '../../widgets/common_button.dart';
import '../../widgets/common_widgets.dart';
import '../../widgets/text_widget.dart';
import 'login_screen.dart';

class OtpVerifyScreen extends StatefulWidget {
  const OtpVerifyScreen({Key? key}) : super(key: key);

  @override
  State<OtpVerifyScreen> createState() => _OtpVerifyScreenState();
}

class _OtpVerifyScreenState extends BaseStatefulWidgetState<OtpVerifyScreen> {


  @override
  void initState() {
    // TODO: implement initState

    super.initState();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }
  @override
  Color? get scaffoldBgColor => colorWhite;
  @override
  bool get shouldHaveSafeArea => false;
  @override
  bool get isBackgroundImage => true;
  @override
  bool get extendBodyBehindAppBar => true;

  @override
  PreferredSizeWidget? buildAppBar(BuildContext context) {
    return  onlyLeadIcAppbar(onTap : goBack);
  }

  @override
  Widget buildBody(BuildContext context) {
    OtpVerifyViewModel otpVerifyViewModel =
    context.read<OtpVerifyViewModel>();
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return ConstrainedBox(
          constraints: BoxConstraints(minHeight: constraints.maxHeight),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                    width: constraints.maxWidth,
                    child: Image.asset(otpVerifyViewModel.isReceiveOtp == true ?
                    imgOtpMsgBackground :imgOtpBackground)),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 7.h),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextWidget(
                        text: strOtpVerify,
                        fontSize: 26.sp,
                        color: color13F,
                        fontWeight: FontWeight.w800,
                      ),
                      heightBox(4.h),
                      TextWidget(
                        text: strSendYou4Digit,
                        fontSize: 16.sp,
                        color: color080,
                        fontWeight: FontWeight.w400,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,

                        children: [
                          TextWidget(
                            text: strOtp,
                            fontSize: 16.sp,
                            color: color080,
                            fontWeight: FontWeight.w400,
                          ),
                          widthBox(8.w),
                          TextWidget(
                            text: strOtpOnMail,
                            fontSize: 16.sp,
                            color: color13F,
                            fontWeight: FontWeight.w600,
                          ),
                        ],
                      ),
                      heightBox(24.h),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 19.0),
                        child: PinCodeTextField(
                          pinTheme: PinTheme(
                            borderRadius: BorderRadius.circular(16),
                            shape: PinCodeFieldShape.box,
                            borderWidth: 0,
                            fieldHeight: 60.h,
                            fieldWidth: 64.w,
                            activeColor: colorFF3,
                            selectedColor : colorFF3,
                            selectedFillColor: colorFF3,
                            activeFillColor: colorFF3,
                            inactiveColor: colorFF3,
                            inactiveFillColor: colorFF3,

                          ),
                          // onCompleted: ()=>,
                          controller: otpVerifyViewModel.otpController,
                          autoDisposeControllers : false,
                          keyboardType:TextInputType.number ,
                          textStyle: TextStyle(
                            color: color239,
                            fontSize: 27.sp,
                            fontFamily: strFontName,
                            fontWeight: FontWeight.w500,

                          ),
                          cursorColor: color239,
                          onChanged: (String value) {  },
                          length: 4,
                          appContext: context,
                          enableActiveFill: true,
                        ),
                      ),

                      heightBox(52.h),
                      CommonButton(
                        text: strConfirm,
                        width: constraints.maxWidth,
                        onTap: ()=>push(context, enterPage: const SetProfileScreen()), // merge otp textField
                      ),

                      heightBox(67.h),

                      TextWidget(
                        onTap: () {},
                        text: strResendOtp,
                        fontSize: 14.sp,
                        color: color080,
                        fontWeight: FontWeight.w500,
                      ),
                      heightBox(68.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextWidget(
                            text: strBackToLogin,
                            fontSize: 12.sp,
                            color: color080,
                            fontWeight: FontWeight.w500,
                          ),
                          widthBox(6.w),
                          TextWidget(
                            onTap: ()=>push(context, enterPage: const LoginScreen()),
                            text: strLogin,
                            fontSize: 12.sp,
                            color: color13F,
                            fontWeight: FontWeight.w600,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
