import 'package:casarancha/screens/auth/login_screen.dart';
import 'package:casarancha/view_models/forgot_pwd_model_view.dart';
import 'package:casarancha/view_models/home_vm/home_screen_view_model.dart';
import 'package:casarancha/view_models/login_screen_view_model.dart';
import 'package:casarancha/view_models/otp_verify_view_model.dart';
import 'package:casarancha/view_models/signup_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'utils/app_constants.dart';
import 'view_models/set_profile_sc_view_model.dart';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return ScreenUtilInit(
        designSize: const Size(375, 812),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (BuildContext context, Widget? child) => MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => LoginScreenViewModel()),
            ChangeNotifierProvider(create: (_) => SignupScreenViewModel()),
            ChangeNotifierProvider(create: (_) => ForgotPwdScreenViewModel()),
            ChangeNotifierProvider(create: (_) => OtpVerifyViewModel()),
            ChangeNotifierProvider(create: (_) => SetProfileScViewModel()),
            ChangeNotifierProvider(create: (_) => HomeScreenViewModel()),
          ],
          child: MaterialApp(
            navigatorKey: rootNavigatorKey,
            debugShowCheckedModeBanner: false,
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                ),
                home: const LoginScreen(),
              ),
        ));
  }
}
