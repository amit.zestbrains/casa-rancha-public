
import 'package:casarancha/resources/strings.dart';
import 'package:casarancha/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../resources/color_resources.dart';
import '../resources/image_resources.dart';

class CommonAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final String? prefixIconName;
  final bool? shouldShowBackButton;
  final PreferredSizeWidget? bottom;
  final bool? isPrefixIcon;
  final Widget? leading;
  final Widget? prefixWidget;
  final bool automaticallyImplyLeading;
  final GestureTapCallback? onTapPrefix;
  final GestureTapCallback? onPressBack;
  final Color? statusBarColor;
  final GestureTapCallback? onTapAction;

  const CommonAppBar(
      {Key? key,
      required this.title,
      this.shouldShowBackButton = true,
      this.bottom,
      this.isPrefixIcon,
      this.prefixIconName,
      this.onTapPrefix,
      this.onPressBack,
      this.automaticallyImplyLeading = true,
      this.leading,
      this.statusBarColor,
      this.prefixWidget,
      this.onTapAction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent, // status bar color
        statusBarIconBrightness: Brightness.light,
        // systemNavigationBarColor: colorFA,
        systemNavigationBarIconBrightness: Brightness.dark,
      ),
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      automaticallyImplyLeading: automaticallyImplyLeading,
      toolbarHeight: 70,
      title: TextWidget(
        text: title,
        color: colorWhite,
        fontWeight: FontWeight.w400,
        fontSize: 18.sp,
      ),
      centerTitle: true,
      leading: (shouldShowBackButton ?? true)
          ? leading ??
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: onPressBack ??
                    () {
                      if (Navigator.canPop(context)) {
                        Navigator.pop(context);
                      }
                    },
                child: Container(
                  height: 5,
                  width: 5,
                  margin: const EdgeInsets.only(left: 22),
                  // padding: const EdgeInsets.only(bottom: 12, right: 5, left: 5, top: 10),
                  // child: SvgPicture.asset(icArrowLeft, color: colorWhite),
                ),
              )
          : null,
      leadingWidth: 55,
      actions: [
        prefixIconName != null
            ? Container(
                margin: const EdgeInsets.only(right: 22),
                padding: const EdgeInsets.only(right: 5, left: 5, top: 25),
                child: TextWidget(
                  text: prefixIconName,
                  color: colorWhite,
                  fontSize: 14.sp,
                  onTap: onTapAction,
                ),
              )
            : prefixWidget ?? const SizedBox.shrink(),
      ],
      bottom: bottom,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(90);
}

statusBarDarkMode(){
  return SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness : Brightness.dark,
      statusBarIconBrightness : Brightness.dark
  ));
}

whiteBgAppBar({String? title, double? fontSize,FontWeight? fontWeight ,
  Color? backgroundClr, double? elevation , String? leadIconSvg ,GestureTapCallback? onTapLeadIc ,
double? leadHorPadding
}){
  return AppBar(
    backgroundColor:  Colors.white,
    systemOverlayStyle : backgroundClr ==null ?  statusBarDarkMode() : null,
    centerTitle: true,
    title:  title != null ? Text(title ,style:  TextStyle(
        color: color13F,
      fontFamily: strFontName,
      fontWeight: fontWeight ?? FontWeight.w600,
      fontSize: fontSize ?? 16.sp
    ),): null,
    elevation: elevation ?? 5,
    leading: leadIconSvg != null ? Padding(
      padding:  EdgeInsets.symmetric(horizontal: leadHorPadding ?? 0, ),
      child: GestureDetector(
        onTap: onTapLeadIc,
        child: SvgPicture.asset(leadIconSvg,
          height: 32.h,
          width: 32.w,),
      ),
    ) : null,
  );
}

PreferredSizeWidget onlyLeadIcAppbar({GestureTapCallback? onTap}){
  return AppBar(
      systemOverlayStyle:const SystemUiOverlayStyle(
          statusBarBrightness : Brightness.dark,
          statusBarIconBrightness : Brightness.dark
      ),
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      leadingWidth: 80.w,
      leading: Padding(
        padding: const EdgeInsets.all(8),
        child: GestureDetector(
            onTap: onTap,
            child: SvgPicture.asset(icLeftArrow)),
      )
  );
}
