import 'package:casarancha/resources/image_resources.dart';
import 'package:casarancha/widgets/text_editing_widget.dart';
import 'package:casarancha/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../resources/color_resources.dart';

Widget heightBox(double height) {
  return SizedBox(height: height);
}

Widget widthBox(double width) {
  return SizedBox(width: width);
}

Widget horizonLine({required double width, double? horizontalMargin}) {
  return Container(
      margin: EdgeInsets.symmetric(horizontal: horizontalMargin ?? 0),
      color: colorDDC,
      height: 1.h,
      width: width);
}

Widget svgImgButton({required String svgIcon, GestureTapCallback? onTap}) {
  return GestureDetector(onTap: onTap, child: SvgPicture.asset(svgIcon));
}

Widget dateDropDown(
    {double? height, double? width, String? text, GestureTapCallback? onTap}) {
  return Expanded(
    child: GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        padding: const EdgeInsets.all(17),
        decoration: BoxDecoration(
          color: colorFF3,
          borderRadius: BorderRadius.circular(16.0),
          // border: Border.all(color: colorF73 ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextWidget(
              text: text ?? "",
              fontSize: 14.sp,
              color: color080,
              fontWeight: FontWeight.w400,
            ),
            widthBox(15.w),
            SvgPicture.asset(icDropDown)
          ],
        ),
      ),
    ),
  );
}

Widget otpTextField({context, required TextEditingController controller , required bool isFocusOnNext}) {
  return TextEditingWidget(
    height: 60.h,
    width: 64.w,
    controller: controller,
    isShadowEnable: false,
    color: colorFF3,
    textAlign: TextAlign.center,
    textInputType: TextInputType.number,
      fontSize: 27.sp,
    fontWeightText: FontWeight.w500 ,
    textInputAction: TextInputAction.next,
    onEditingComplete: () => FocusScope.of(context).nextFocus(),
    maxLength: 1,

    onChanged: (val) {
      if (val.length == 1) {
          isFocusOnNext
              ? FocusScope.of(context).nextFocus()
              : FocusScope.of(context).unfocus();
        }
      }
  );
}
