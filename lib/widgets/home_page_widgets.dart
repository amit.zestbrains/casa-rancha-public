import 'package:casarancha/resources/color_resources.dart';
import 'package:casarancha/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../resources/image_resources.dart';
import 'common_widgets.dart';

enum PostType { image, video, music }

Widget feedCardHome(
    {required double width,
    required String imgUserNet,
    required bool isVerify,
    required bool isLikedPost,
    required String userName,
    required String lastSeen,
      int? likeCount,
      int? commentCount,
      postType, imgPost,
    }) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 16.h, horizontal: 20.w),
    height: 325.h,
    width: width,
    decoration: const BoxDecoration(
        color: colorWhite,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 5.0,
          ),
        ]),
    child: Padding(
      padding: const EdgeInsets.all(14),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              CircleAvatar(
                  radius: 20.r,
                  backgroundImage: Image.network(imgUserNet).image),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        TextWidget(
                          text: userName,
                          color: color221,
                          fontWeight: FontWeight.w500,
                          fontSize: 14.sp,
                        ),
                        widthBox(6.w),
                        isVerify ? SvgPicture.asset(icVerifyBadge) : Container()
                      ],
                    ),
                    TextWidget(
                      text: lastSeen,
                      color: color55F,
                      fontWeight: FontWeight.w400,
                      fontSize: 11.sp,
                    ),
                  ],
                ),
              ),
              const Spacer(),
              svgImgButton(svgIcon: icCardMenu, onTap: () {})
            ],
          ),
          heightBox(13.h),
          ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: postType == PostType.music
                ? Container()
                : postType == PostType.video
                    ? Container()
                    : Image.network(
                        imgPost,
                        height: 160.h,
                        width: width,
                        fit: BoxFit.fill,
                      ),
          ),
          heightBox(13.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              svgImgButton(svgIcon: isLikedPost ? icLikeRed : icLikeWhite, onTap: () {}),
              TextWidget(
                text: lastSeen,
                color: color55F,
                fontWeight: FontWeight.w400,
                fontSize: 11.sp,
              ),
              svgImgButton(svgIcon: isLikedPost ? icLikeRed : icLikeWhite, onTap: () {}),
              svgImgButton(svgIcon: isLikedPost ? icLikeRed : icLikeWhite, onTap: () {})
            ],
          ),
        ],
      ),
    ),
  );
}
