import 'package:flutter/material.dart';

import '../resources/color_resources.dart';
import '../resources/image_resources.dart';

class LoginScreenViewModel with ChangeNotifier {
  bool passwordVisible = true;
  FocusNode emailFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();
  String? icEmailSvg , icPasswordSvg;
  Color? emailFillClr , passwordFillClr , emailBorderClr , passwordBorderClr;
   TextEditingController emailController = TextEditingController();
   TextEditingController passwordController = TextEditingController();
   GlobalKey<FormState> globalLoginFormKey = GlobalKey<FormState>();
  setPwdVisibility() {
    passwordVisible = !passwordVisible;
    notifyListeners();
  }


  onFieldFocusChange() {
    notifyListeners();
  }

  setEmailFocusChange() {
    if(emailFocus.hasFocus){
      icEmailSvg = icSelectEmail;
      emailFillClr = colorFDF;
      emailBorderClr = colorF73;
    }else{
      icEmailSvg = icDeselectEmail;
      emailFillClr = colorFF3;
      emailBorderClr = null;
    }
    notifyListeners();
  }

  setPwdFocusChange() {
    if(passwordFocus.hasFocus){
      icPasswordSvg = icSelectLock;
      passwordFillClr = colorFDF;
      passwordBorderClr = colorF73;
    }else{
      icPasswordSvg = icDeselectLock;
      passwordFillClr = colorFF3;
      passwordBorderClr = null;
    }

    notifyListeners();
  }


}
