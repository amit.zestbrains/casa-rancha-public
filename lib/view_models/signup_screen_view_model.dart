import 'package:flutter/material.dart';

import '../resources/color_resources.dart';
import '../resources/image_resources.dart';

class SignupScreenViewModel with ChangeNotifier{
  bool passwordVisible = true;
  bool confirmPasswordVisible = true;
  FocusNode emailFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();
  FocusNode confirmPwdFocus = FocusNode();
  FocusNode userNameFocus = FocusNode();
  String? icUsernameSvg ,icEmailSvg , icPasswordSvg , icConfirmPwdSvg ;
  Color? usernameFillClr , emailFillClr , passwordFillClr ,confirmPwdFillClr,
         usernameBorderClr, emailBorderClr , passwordBorderClr, confirmPwdBorderClr;

   GlobalKey<FormState> globalSignupFormKey = GlobalKey<FormState>();
    TextEditingController usernameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
   TextEditingController passwordController = TextEditingController();
   TextEditingController confirmPwdController = TextEditingController();

  setPwdVisibility(){
    passwordVisible = !passwordVisible;
    notifyListeners();
  }
  setConfirmPwdVisibility(){
    confirmPasswordVisible = !confirmPasswordVisible;
    notifyListeners();
  }
  setUsernameFocusChange() {
    if(userNameFocus.hasFocus){
      icUsernameSvg = icSelectUser;
      usernameFillClr = colorFDF;
      usernameBorderClr = colorF73;
    }else{
      icUsernameSvg = icDeselectUser;
      usernameFillClr = colorFF3;
      usernameBorderClr = null;
    }
    notifyListeners();
  }
  setEmailFocusChange() {
    if(emailFocus.hasFocus){
      icEmailSvg = icSelectEmail;
      emailFillClr = colorFDF;
      emailBorderClr = colorF73;
    }else{
      icEmailSvg = icDeselectEmail;
      emailFillClr = colorFF3;
      emailBorderClr = null;
    }
    notifyListeners();
  }
  setPwdFocusChange() {
    if(passwordFocus.hasFocus){
      icPasswordSvg = icSelectLock;
      passwordFillClr = colorFDF;
      passwordBorderClr = colorF73;
    }else{
      icPasswordSvg = icDeselectLock;
      passwordFillClr = colorFF3;
      passwordBorderClr = null;
    }
    notifyListeners();
  }
  setConfirmPwdFocusChange() {
    if(confirmPwdFocus.hasFocus){
      icConfirmPwdSvg = icSelectLock;
      confirmPwdFillClr = colorFDF;
      confirmPwdBorderClr = colorF73;
    }else{
      icConfirmPwdSvg = icDeselectLock;
      confirmPwdFillClr = colorFF3;
      confirmPwdBorderClr = null;
    }
    notifyListeners();
  }

}