import 'package:flutter/material.dart';

class SetProfileScViewModel  with ChangeNotifier{
   TextEditingController firstNameController = TextEditingController();
   TextEditingController lastNameController = TextEditingController();
   TextEditingController bioController = TextEditingController();
   String bioTxtCount = "0";
    getBioTextCount(){
      bioTxtCount = bioController.text.length.toString();
      notifyListeners();
   }
}