import 'package:flutter/material.dart';
import '../resources/color_resources.dart';

import '../resources/image_resources.dart';

class ForgotPwdScreenViewModel with ChangeNotifier{
   TextEditingController emailController = TextEditingController();
   FocusNode emailFocus = FocusNode();
  String? icEmailSvg ;
  Color? emailFillClr ,emailBorderClr;
  setEmailFocusChange() {

    if(emailFocus.hasFocus){
      icEmailSvg = icSelectEmail;
      emailFillClr = colorFDF;
      emailBorderClr = colorF73;
    }else{
      icEmailSvg = icDeselectEmail;
      emailFillClr = colorFF3;
      emailBorderClr = null;
    }
    notifyListeners();
  }

}